/*************************** Helpers ****************************/

function bindData(group) {

    deselectAllGroups();

    //console.log(group);

    group.setAttr("isSelected", true);

    for (var child of group.children) {

        // if (child.className === "Circle") {
        //     child.attrs.stroke = '#C05000';
        // }


        if (child.className === "Circle")
            console.log(child);
    }
}

// Convert text color to hex color
function translateColor(colorToTranslate) {

    let color = {
        'GRAY'() {
            return '#7E8C8D';
        },
        'SILVER'() {
            return '#BCC3C7';
        },
        'BLUE'() {
            return '#0982B9';
        },
        'CYAN'() {
            return '#00A7DB';
        },
        'GREEN'() {
            return '#00AD61';
        },
        'PURPLE'() {
            return '#9249AD';
        },
        'COPPER'() {
            return '#893A00';
        },
        'RED'() {
            return '#C73927';
        },
        'MAGENTA'() {
            return '#E62C87';
        },
        'ORANGE'() {
            return '#ED7D1B';
        },
        'GOLD'() {
            return '#DFA625';
        },
        'YELLOW'() {
            return '#F5C205';
        },
        'WARMGREY'() {
            return '#ff7f7f';
        },
        'default'() {
            return '#F06E30';
        }
    };

    return (color[colorToTranslate] || color['default'])();
}

// Return a group to delete
function removeGroup(array, element) {
    return array.filter(e => e !== element);

    // TODO : FIX
}

// Deselect all groups
function deselectAllGroups() {

    for (var group of groupCollection) {

        for (var child of group.children) {
            if (child.nodeType === "Shape") {
                child.attrs.stroke = '#CCC6C0';
            }
        }

        group.attrs.isSelected = false;
    }
}

// Populate the settings with the main table fields
function choiceMainTableSelect(select) {

    console.log(select.options[select.selectedIndex].text);

    var test = select.options[select.selectedIndex].text;

    $("#joinTableBtn").removeClass('disabled');
    $("#right_col").show();

    $("#sortable").empty();

    $.each(configTest.uFields, function(key, val) {

        if (val.table === test) {
            $("#sortable").append($("<li class='ui-state-default'>" + val.labelEn + " (" + val.table + ")</li>"));
        }
    });

    $("#sortable").sortable('refresh');
}

// Populate the main table settings with the joind fields
function choiceJoinTypeSelect(select) {

    console.log(select.options[select.selectedIndex].text);

    var $li1 = $("<li class='ui-state-default'>Item 1 (" + select.options[select.selectedIndex].text + ")</li>");
    var $li2 = $("<li class='ui-state-default'>Item 2 (" + select.options[select.selectedIndex].text + ")</li>");
    var $li3 = $("<li class='ui-state-default'>Item 3 (" + select.options[select.selectedIndex].text + ")</li>");

    $("#sortable").append($li1);
    $("#sortable").append($li2);
    $("#sortable").append($li3);
    $("#sortable").sortable('refresh');
}

// Returns the selected group
function getSelectedGroup() {
    groupCollection.find(function (group) {
        return group.attrs.isSelected === true;
    });
}