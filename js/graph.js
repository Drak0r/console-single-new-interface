/*************************** Graph ****************************/

// Convert GraphNode to TreeNode
function GraphToTree(graph) {

    let treeNode = new TreeNode();

    treeNode.Id = graph.Id;
    treeNode.entity = graph.entity;
    treeNode.split = graph.split;
    treeNode.Parent = null;

    for (let child of graph.Children) {

        let treeChild = GraphToTree(child);
        treeNode.Children.push(treeChild);
        treeChild.Parent = treeNode;
    }

    return treeNode;
}

// Apply an offset if there are multiple graphs
function applyOffset(node, offset) {

    // if (node.Parent === null && node.Children.length === 0)
    //     node.Y += offset - 500;
    // else
    //     node.X += offset;

            node.X += offset;


    for (let child of node.Children) {
        applyOffset(child, offset);
    }
}

// Build the graph based on the uEntities, uRelations, uSplit tabs
function BuildGraph(uEntities, uRelations, uSplit) {

    let nodes = [];

    let getEntity = relation => uEntities.find(x => x.name === relation.entity),
        getForeignEntity = relation => uEntities.find(x => x.name === relation.foreignEntity),
        getChild = relation => nodes.find(x => x.Id === relation.entity),
        getParent = relation => nodes.find(x => x.Id === relation.foreignEntity),
        getSplit = (uSplit, parent) => uSplit.filter(x => x.entity === parent.Id),
        getInnerParent = (nodes, id) => nodes.find(x => x.Id === id),
        getParentForSplit = (nodes, entity) => nodes.find(x => x.Id === entity.name);

    for (let relation of uRelations) {

        let uEntity = getEntity(relation),
            uForeignEntity = getForeignEntity(relation);

        if (uEntity === undefined || uForeignEntity === undefined)
            continue;

        let child = getChild(relation);

        if (child === undefined) {

            child = new GraphNode();

            child.Id = relation.entity;
            child.entity = uEntity;
            child.split = null;

            nodes.push(child);
        }

        let parent = getParent(relation);

        if (parent === undefined) {

            parent = new GraphNode();

            parent.Id = relation.foreignEntity;
            parent.entity = uForeignEntity;
            parent.split = null;

            nodes.push(parent);
        }

        parent.Children.push(child);
        child.Parents.push(parent);

    }

     for (let entity of uEntities) {

        var parent = getParentForSplit(nodes, entity);

        if (parent === undefined) {

            parent = new GraphNode();

            parent.Id = entity.name;
            parent.entity = entity;
            parent.split = null;

            nodes.push(parent);
        }

        let splits = getSplit(uSplit, parent);

        for (let split of splits) {

            let id = split.entity + "." + split.uniqueName;
            let inter_parent = getInnerParent(nodes, id);

            if (inter_parent === undefined) {

                inter_parent = new GraphNode();

                inter_parent.Id = id;
                inter_parent.entity = parent.entity;
                inter_parent.split = split;
            }

            parent.Children.push(inter_parent);
            inter_parent.Parents.push(parent);
        }
    }

    return nodes.filter(x => x.Parents.length === 0);
}

// Draw the graph
function drawGraph(layer, node, parent_entity) {

    for (let child of node.Children) {

        let child_entity = new Entity(layer, child.X + 100, child.Y + 100, child);

        parent_entity.linkTo(layer, child_entity, node);
        drawGraph(layer, child, child_entity);
    }

    // TODO : Manage large maps
}