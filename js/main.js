/*************************** Ready ****************************/

// First call
$(document).ready(function() {
    Init();
});

// Page initialization
function Init() {

    /**
    * SideNav
    **/

    $('.button-collapse').sideNav({
        menuWidth: '70%',
        edge: 'right',
        closeOnClick: false,
        draggable: true,
        onOpen: function(el) {},
        onClose: function(el) {},
    });

    /**
    * Modal
    **/

    $('.modal').modal({
        dismissible: true,
        opacity: 0.5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '4%',
        endingTop: '10%',
        ready: function(modal, trigger) {},
        complete: function() {}
    });

    /**
    * Select2
    **/

    $('.modalSelect').select2({
      width: '100%',
      height: '25px'
    });

    /**
    * Collapsible
    **/

    $('.collapsible').collapsible();

    /**
    * Sortable
    **/

    $(".column").sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
    });

    $("#sortable").sortable({
        placeholder: "ui-state-highlight"
    });

    $("#sortable").disableSelection();

    /**
    * Portlet
    **/

    $(".portlet")
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

    $(".portlet-toggle").on("click", function() {

        var icon = $(this);

        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });

    /**
    * ContextMenu
    **/

    $.contextMenu({
        selector: '.context-menu-entity',
        trigger: 'left',
        callback: function(item, option) {

            let selectedItem = {
                'data'() {
                    $('#entitySideNavBtn').sideNav('show');
                },
                'settings'() {
                    $("#entitySettingsSideNavBtn").sideNav('show');
                },
                'condition'() {
                    $('#conditionModal').modal('open');
                },
                'delete'() {
                    $('#deleteModal').modal('open');
                },
                'color'() {
                    $('#colorModal').modal('open');
                },
                'icon'() {
                    $('#iconModal').modal('open');
                },
                'relation'() {

                    $('#relationModal').modal('open');

                    $.each(configTest.uEntities, function(key, val) {
                        $("#foreignEntity").append('<option id="' + val.name + '">' + val.name + '</option>');
                    });
                },
                'split'() {
                    $("#splitModal").modal('open');
                },
                'default'() {
                    return `Error with this item : ${item}.`;
                }
            };

            return (selectedItem[item] || selectedItem['default'])();
        },
        items: {
            "data": {name: "Data"},
            "fold1a": {
                "name": "Look and feel",
                "items": {
                    "color": {"name": "Color"},
                    "icon": {"name": "Icon"},
                }
            },
            "settings": {name: "Settings"},
            "sep1": "---------",
            "relation": {name: "Relation", disabled: function() {

                if (groupCollection.length > 1) {
                    return false;
                } else {
                    return true;
                }
            }},
            "split": {name: "Split"},
            "sep2": "---------",
            "delete": {name: "Delete"},
            "sep3": "---------",
            "quit": {name: "Quit"}
        }
    });

    /**
    * Zoom
    **/

    var scaleBy = 1.10;

    window.addEventListener('wheel', (e) => {

        e.preventDefault();

        var oldScale = stage.scaleX();

        var mousePointTo = {
            x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
            y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale,
        };

        var newScale = e.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

        // if (newScale > 1.20)
        //     newScale = 1.20;
        // else if (newScale < 0.40)
        //     newScale = 0.40;

        stage.scale({ x: newScale, y: newScale });

        var newPos = {
            x: - (mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale,
            y: - (mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale
        };

        stage.position(newPos);
        stage.batchDraw();
    });

    /**
    * Add Konva stage to layer
    **/

    stage.add(layer);

    /**
    * Misc
    **/

    /* Graph logic */

    const config = configTest5;

    let graphs = BuildGraph(config.uEntities, config.uRelation, config.uSplit),
        offset = 0;

    for (var graph of graphs) {

        let root = GraphToTree(graph);
        let graphSize = NodeHelper.calculateCoordinates(root);

        applyOffset(root, offset);

        offset += graphSize.Width + 150;

        let root_entity = new Entity(layer, root.X + 100, root.Y + 100, root);

        drawGraph(layer, root, root_entity);
        layer.draw();
    }

    //var json = stage.toJSON();
    // console.log("Stage : ");
    // console.log(JSON.parse(json));
}