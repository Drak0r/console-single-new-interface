/*************************** Source events ****************************/

$(document).on('click', '#newConfiguration', function () {

    console.log('source');
    const source2 = new Source("/Api/DataSourceConfiguration/3072");
    console.log(source2.getMetadata());
});


/*************************** Misc ****************************/

// Create materialize toast
$(document).on('click', '#saveCurrentConfiguration', function () {

    Materialize.toast('Configuration saved.', 4000);

    // TODO : call postMetadata()
});

// Add an entity to the graph
$(document).on('click', '#addEntity', function () {

    console.log('addEntity');

    // TODO : create a method adding new entity
});

// Add a new relation in the configuration
$(document).on('click', '#addRelation', function (e) {

    console.log('addRelation');

    // TODO : create a method adding new relation
});

// Add a join table to a main table
$(document).on('click', '#addJoin', function (e) {

    console.log('addJoin');

    // TODO : create a join table
    // TODO : display joined field inside main table sheet settings
    // TODO : display an indication on the map that a join is set
    // TODO : delete / edit the join
});

// Add a new split in the configuration
$(document).on('click', '#addSplit', function (e) {

    console.log('addSplit');

    // TODO : create a method adding new split
});

// Delete an entity and all that is associated to it
$(document).on('click', '#deleteElement', function () {

    console.log('deleteElement');

    var element = getSelectedGroup();

    removeGroup(groupCollection, element);

    element.destroy();
    layer.draw();

    // TODO : remove all elements where this entity is present
});