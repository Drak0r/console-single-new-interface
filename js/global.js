/*************************** Variables ****************************/

// Constant for graph drawing
const FLOWER_BASE_RADIUS = 50;
const FLOWER_FATHER_SON_INC_FACTOR = 1.1;

// Stores groups
let groupCollection = [];

// Konva stage
let stage = new Konva.Stage({
    container: 'dragAndDropContainer',
    width: window.innerWidth,
    height: window.innerWidth,
    draggable: true
});

// Konva layer
let layer = new Konva.Layer();