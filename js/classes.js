/*************************** Source ****************************/

class Source {

    constructor(url) {
        this.url = url;
    }

    getMetadata() {

        $.get(this.url).then(function (response) {
            console.log(response);
        }, function (error) {
            console.error(error);
        });
    }

    postMetadata() {

        $.post(this.url).then(function (response) {
            console.log(response);
        }, function (error) {
            console.error(error);
        });
    }
}


/*************************** Entity ****************************/

class Entity {

    constructor(layer, x, y, node) {

        /**
        * Konva objects
        **/

        this.circle = new Konva.Circle({
            x: x,
            y: y,
            radius: node.split !== null ? 15 : 25,
            fill: node.split !== null ? translateColor(node.split.color) : translateColor(node.entity.color),
            stroke: node.split !== null ? translateColor(node.split.color) : translateColor(node.entity.color),
            strokeWidth: 0.1,
            shadowColor: 'black',
            shadowBlur: 8,
            shadowOffset: { x: 2, y: 2 },
            shadowOpacity: 0.4
        });

        let tooltip = new Konva.Label({
            x : x,
            y : y + 35,
            opacity: 0.75
        });

        tooltip.add(new Konva.Tag({
            fill: 'white',
            cornerRadius: 25,
            lineJoin: 'round',
            shadowColor: 'black',
            shadowBlur: 10,
            shadowOffset: 10,
            shadowOpacity: 0.5
        }));

        tooltip.add(new Konva.Text({
            text: node.split !== null && node.split.labelFr !== "" ? "Split : " + node.split.labelFr : node.entity.labelFr,
            fontFamily: 'Calibri',
            fontSize: 18,
            padding: 5,
            fill: 'black'
        }));

        tooltip.attrs.x = x - tooltip.children[0].attrs.width / 2;

        let gleam = new Image(),
            bubbleGleam = new Konva.Image ({
            x: node.split !== null ? x - 15 : x - 25,
            y: node.split !== null ? y - 15 : y - 25,
            image: gleam,
            width: node.split !== null ? 30 : 50,
            height: node.split !== null ? 30 : 50
        });

        gleam.src = 'img/icons/bg_bubble_gleam.png';

        let icon = new Image();
        let bubbleicon = new Konva.Image ({
            x: node.split !== null ? x - 7.5 : x - 12.5,
            y: node.split !== null ? y - 7.5 : y - 12.5,
            image: icon,
            width: node.split !== null ? 15 : 25,
            height: node.split !== null ? 15 : 25
        });

        icon.src = 'img/icons/' + node.entity.icon + '.png';

        let group = new Konva.Group({
            draggable: false,
            isSelected: false,
        });

        group.treeNode = node;

        group.add(this.circle)
             .add(tooltip)
             .add(bubbleGleam)
             .add(bubbleicon);

        groupCollection.push(group);

        layer.add(group);

        /**
        * Konva events
        **/

        group.on("mouseover", function() {
            document.body.style.cursor = "pointer";
        });

        group.on("mouseout", function() {
            document.body.style.cursor = "default";
        });

        group.on("click", function(e, i) {

            //bindData(this);

            console.log(this);

            $('.context-menu-entity').contextMenu({
                x : e.evt.layerX + 35,
                y : e.evt.layerY + 95
            });

            layer.draw();
        });
    }

    linkTo(layer, entity2, node) {

        /**
        * Konva line
        **/

        var line = new Konva.Line({
            points: [this.circle.getX(), this.circle.getY(), entity2.circle.getX(), entity2.circle.getY()],
            stroke: node.split !== null ? translateColor(node.split.color) : translateColor(node.entity.color),
            strokeWidth: 3,
            shadowColor: 'black',
            shadowBlur: 10,
            shadowOffset: {x : 5, y : 5},
            shadowOpacity: 0.5,
            draggable: false
        });

        layer.add(line);
        line.moveToBottom();
    }
}


/*************************** Graph ****************************/

class TreeNode {

    constructor() {
        this.X = 0;
        this.Y = 0;
        this.PolarDist = 0;
        this.PolarAngle = 0;
        this.Children = [];
        this.Parent = {};
        this.entity = {};
        this.split = {};
    }
}

class GraphNode {

    constructor() {
        this.Children = [];
        this.Parents = [];
        this.entity = {};
        this.split = {};
    }
}

class GraphSize {

    constructor() {
        this.Width = 0;
        this.Height = 0;
    }
}

class NodeHelper {

    static calculateCoordinates(root) {

        let distanceBetweenFatherAndSon = FLOWER_BASE_RADIUS;
        let endAngle = 0;

        do {
            endAngle = this.flowerPlace(root, 0, 0, distanceBetweenFatherAndSon);
            if (endAngle > 2 * Math.PI) {
                distanceBetweenFatherAndSon = distanceBetweenFatherAndSon * FLOWER_FATHER_SON_INC_FACTOR;
            }
        } while (endAngle > 2 * Math.PI);

        this.flowerSymmetry(root);
        this.polarToPlanar(root);

        let nodes = this.nodeToList(root);

        const minX = Math.min(...nodes.map(x => x.X));
        const minY = Math.min(...nodes.map(x => x.Y));
        const maxX = Math.max(...nodes.map(x => x.X));
        const maxY = Math.max(...nodes.map(x => x.Y));

        for (let node of nodes) {
            node.X = node.X - minX;
            node.Y = node.Y - minY;
        }

        let graph_size = new GraphSize();

        graph_size.Width = maxX - minX;
        graph_size.Height = maxY - minY;

        return graph_size;
    }

    static nodeToList(rootNode) {

        let nodes = [];
        this.traverseNode(rootNode, nodes);

        return nodes;
    }

    static traverseNode(parent, nodes) {

        nodes.push(parent);

        for (let child of parent.Children) {
            this.traverseNode(child, nodes);
        }
    }

    static flowerPlace(node, radius, startAngle, distanceBetweenFatherAndSon) {

        const apexAngle = 2 * Math.asin(FLOWER_BASE_RADIUS / (FLOWER_BASE_RADIUS + radius));
        let endAngle = startAngle + apexAngle;
        const toChildRadius = radius + 2 * FLOWER_BASE_RADIUS + distanceBetweenFatherAndSon;

        node.PolarDist = radius + FLOWER_BASE_RADIUS;
        node.PolarAngle = startAngle;

        for (let child of node.Children) {

            const endAngleSubTree = NodeHelper.flowerPlace(child, toChildRadius, startAngle, distanceBetweenFatherAndSon);

            endAngle = Math.max(endAngle, endAngleSubTree);
            startAngle = endAngleSubTree;
        }

        return endAngle;
    }

    static flowerSymmetry(root) {

        let angleMin = root.PolarAngle;
        let angleMax = angleMin;

        for (let child of root.Children) {

            angleMin = angleMin > child.PolarAngle ? child.PolarAngle : angleMin;
            angleMax = angleMax < child.PolarAngle ? child.PolarAngle : angleMax;

            this.flowerSymmetry(child);
        }

        root.PolarAngle = (angleMin + angleMax) / 2;
    }

    static polarToPlanar(root) {

        for (let child of root.Children) {

            child.X = Math.floor(child.PolarDist * Math.cos(child.PolarAngle));
            child.Y = Math.floor(child.PolarDist * Math.sin(child.PolarAngle));

            this.polarToPlanar(child);
        }
    }
}